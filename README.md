# Summary:
Servers are from AWS EC2 or EKS clusters. Files in the repository have their own purpose as mentioned below.


# Files and their use:
ec2-status-check.py: EC2 server health status is obtained by running this file. The scheduler runs every 5 minutes and reports the results.

eks-status-checks.py: EKS cluster status whether it is active or inactive, it's public endpoint, and kubernetes version are checked with this script.

volume-backups.py: Volumes with prod tags are backed up into snapshots. These snapshots shall be created once a day or a week depending on the needs.

cleanup-snapshots.py: If the snapshots are not cleaned or removed from AWS account it will increase the bill, to avoid un-necessary cost, old snapshots shall be cleaned-up once is a month or so except most recent two snapshots for using them to create volumes if primary volume gets corrupt.

restore-volume-from-snapshot.py: Creating volume from the most recent snapshot for an instance. This is helpful if an instance fails or volume data gets corrupt.

monitor-website.py: The Running application status is verified by python libraries using it's endpoint. If application is not accessible, either container is restarted or server is rebooted and container restarted. This health check is done every 5 minutes.

ec2-monitor-website.py: Creating an EC2 instance, waiting for it to initialize fully, installing docker and nginx application on it, adding required firewall rules, monitoring the application endpoint whether it is successfully started or not. If the website is down for subsequent requests, a restart of container is requested.

add-env-tags.py: Adding tags for EC2 instances in two regions.