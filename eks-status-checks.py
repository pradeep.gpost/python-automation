import boto3

client = boto3.client('eks', region_name="ap-south-1")

response = client.list_clusters()

clusters = response['clusters']
print(clusters)  # should return a list of cluster names in the region

for cluster in clusters:
    response = client.describe_cluster(name=cluster)
    temp = response['cluster']
    version = temp['version']
    endpoint = temp['endpoint']
    status = temp['status']
    print(f"Cluster {cluster} Details: \nversion = {version} \nendpoint = {endpoint} \nstatus = {status}")



