import boto3

ec2_client_mumbai = boto3.client('ec2', region_name="ap-south-1")
ec2_resource_mumbai = boto3.resource('ec2', region_name="ap-south-1")

response = ec2_client_mumbai.describe_instances()
reservations = response['Reservations']

instances_mumbai = []

for reservation in reservations:
    instances = reservation['Instances']
    for ins in instances:
        instances_mumbai.append(ins['InstanceId'])

print(f"Mumbai Instances: {instances_mumbai}")

res = ec2_resource_mumbai.create_tags(
    Resources=instances_mumbai,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'prod'
        },
    ]
)

ec2_client_singapore = boto3.client('ec2', region_name="ap-southeast-1")
ec2_resource_singapore = boto3.resource('ec2', region_name="ap-southeast-1")

response = ec2_client_singapore.describe_instances()
reservations = response['Reservations']

instances_singapore = []

for reservation in reservations:
    instances = reservation['Instances']
    for ins in instances:
        instances_singapore.append(ins['InstanceId'])

print(f"Singapore instances:{instances_singapore}")

res = ec2_resource_singapore.create_tags(
    Resources=instances_singapore,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'dev'
        },
    ]
)