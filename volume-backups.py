import schedule
import boto3

client = boto3.client('ec2')


def create_volume_snapshots():
    response = client.describe_volumes(
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': [
                    'prod',
                ]
            },
        ],
    )
    volumes = response['Volumes']

    for volume in volumes:
        # print(volume['VolumeId'])
        new_snapshot = client.create_snapshot(
            VolumeId=volume['VolumeId']
        )
        print(new_snapshot)


schedule.every(20).seconds.do(create_volume_snapshots)

while True:
    schedule.run_pending()