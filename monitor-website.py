
import requests
import smtplib
import os
import paramiko
import linode_api4
import time
import schedule

# There are no constants as such is python, below can be interpreted as variables -
# - but visually can be treated as constants.
# EMAIL_ADDR, EMAIL_PWD are set as environment variables in the ide configuration
EMAIL_ADDRESS = os.environ.get('EMAIL_ADDR')
EMAIL_PASSWORD = os.environ.get('EMAIL_PWD')
LINODE_TOKEN = os.environ.get("LINODE_TOKEN")


def restart_server_and_container():
    # Restart Linode server
    print("Rebooting the server...")
    client = linode_api4.LinodeClient(LINODE_TOKEN)
    nginx_server = client.load(linode_api4.Instance, 31904310)  # Linode Id
    nginx_server.reboot()

    # timing issue between reboot() and restart_container() as linode server may not be ready yet.
    while True:
        nginx_server = client.load(linode_api4.Instance, 31904310)  # Linode Id
        if nginx_server.status == "running":
            time.sleep(5)  # giving some time (5 secs) for server to get ready state fully after 'running' status
            # Start the application
            restart_container()
            break


def send_notification(email_msg):
    print("Sending email ...")
    with smtplib.SMTP("smtp.gmail.com", 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        message = f"Subject: SITE DOWN\n{email_msg}"
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, message)


def restart_container():
    # restart application (docker restart <container-name>), even though app shows in 'docker ps'
    print("Restarting Application...")
    ssh = paramiko.SSHClient()
    # trust the remote server, auto 'yes'
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # private key location is also added
    ssh.connect(hostname="45.79.121.91", username="root", key_filename="/Users/pradeepch/.ssh/id_rsa")
    stdin, stdout, stderr = ssh.exec_command("docker restart brave_bhaskara")  # return tuple.
    print(stdout.readlines())  # stdout is a file, read content of file using readlines()
    ssh.close()


# If server is down,( Eg: docker container is stopped/down) it does not bother about returning status code,-
# - as it seem to have a bigger problem. (Linode server could be down as well)
# This type of problem is to be handled by exception, send email.
# Stop the docker container on which server is running to see the exception.

def monitor_application():
    try:
        response = requests.get("http://45-79-121-91.ip.linodeusercontent.com:8080/")
        # print(type(response.status_code))
        # status_code is not 200 when application might be running but some issue in the application.
        if response.status_code == 200:
            # if False:
            print("Application is running successfully")
        else:
            print("Application down, fix it")
            # send email from my gmail to your gmail or my email
            msg = f"Application returned {response.status_code}"
            send_notification(msg)
            restart_container()

    except Exception as ex:
        print(f"Connection error happened: {ex}")
        msg = "Application not accessible at all."
        send_notification(msg)
        restart_server_and_container()


schedule.every(5).minutes.do(monitor_application)

while True:
    schedule.run_pending()
