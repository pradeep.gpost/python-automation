import time

import boto3
import paramiko
import requests
import schedule

ec2_client = boto3.client("ec2", region_name="ap-south-1")
ec2_resource = boto3.resource("ec2", region_name="ap-south-1")


def create_instance():
    instances = ec2_resource.create_instances(
        InstanceType="t2.micro",
        KeyName="docker-server",
        ImageId="ami-0f1fb91a596abf28d",
        MaxCount=1,
        MinCount=1,
        TagSpecifications=[
            {
                'ResourceType': 'instance',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': 'my-ec2-instance'
                    },
                ]
            },
        ],
    )
    # Returns list of ec2.instance
    # check docs of ec2.instance to see what attributes are returned.
    # Loop runs one time as there is only one instance created.
    for instance in instances:
        print(f"EC2 instance {instance.instance_id} has been launched")
        instance.wait_until_running()
        print(f"EC2 instance {instance.instance_id} has been started")

        # print(f"Public ip before wait is : {instance.public_ip_address}")

        while True:
            response = ec2_client.describe_instance_status(
                InstanceIds=[instance.instance_id]
            )
            statuses = response['InstanceStatuses']
            # First instance in the list
            status = statuses[0]
            instance_status = status['InstanceStatus']
            system_status = status['SystemStatus']
            # print(f"Instance status: {instance_status['Status']}, System status: {system_status['Status']}")
            if instance_status['Status'] == 'ok' and system_status['Status'] == 'ok':
                print(f"Instance status: {instance_status['Status']}, System status: {system_status['Status']}")
                break

        useful_instance = ec2_resource.Instance(instance.instance_id)
        print(f"Public Ip is: {useful_instance.public_ip_address}")
        return [instance.instance_id, useful_instance.public_ip_address]


def install_docker_nginx_on_ec2(ec2_public_ip_address):
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=ec2_public_ip_address, username='ec2-user', key_filename="/Users/pradeepch/.ssh/docker-server.pem")
    print("Installing docker and nginx application...")
    stdin, stdout, stderr = ssh_client.exec_command("sudo yum update -y && sudo yum install -y docker")
    print(stdout.readlines())
    stdin, stdout, stderr = ssh_client.exec_command("sudo systemctl start docker")
    print(stdout.readlines())
    stdin, stdout, stderr = ssh_client.exec_command("sudo usermod -aG docker ec2-user")
    print(stdout.readlines())
    stdin, stdout, stderr = ssh_client.exec_command("docker run -d -p 8080:80 nginx")
    print(stdout.readlines())
    ssh_client.close()


def add_ingress_rules_to_sg(instance_id):
    response = ec2_client.describe_instances(InstanceIds=[instance_id])
    reservations = response['Reservations']
    for reservation in reservations:
        instances = reservation['Instances']
        for instance in instances:
            sgs = instance['SecurityGroups']
            for sg in sgs:
                sg_id = sg['GroupId']

    print(f"security group id: {sg_id} of instance {instance_id}")

    print("Adding Ingress rules to security group...")
    response = ec2_client.authorize_security_group_ingress(
            GroupId=sg_id,
            IpPermissions=[
                {
                    'FromPort': 22,
                    'IpProtocol': 'tcp',
                    'IpRanges': [
                        {
                            'CidrIp': '49.37.151.70/32',
                            'Description': 'For ssh access to my-ip',
                        },
                    ],
                    'ToPort': 22,
                },
                {
                    'FromPort': 8080,
                    'IpProtocol': 'tcp',
                    'IpRanges': [
                        {
                            'CidrIp': '0.0.0.0/0',
                            'Description': 'To access nginx server at port 8080',
                        },
                    ],
                    'ToPort': 8080,
                },
            ],
    )
    print(response)


def get_public_dns(instance_id):
    response = ec2_client.describe_instances(
        InstanceIds=[instance_id]
    )
    reservations = response['Reservations']
    for reservation in reservations:
        instances = reservation['Instances']
        for inst in instances:
            public_dns_name = inst['PublicDnsName']

        print(f"Public dns name of instance is: {public_dns_name}")
    return public_dns_name


def nginx_status(public_dns_name, public_ip):
    print("Getting nginx application status ...")
    global status_count
    # If changing value of global variable in fn, need to declare as global -
    # - if just reading value, no need to declare as global
    try:
        response = requests.get(f"http://{public_dns_name}:8080")
        print(response.status_code)
        if response.status_code == 200:  # OK status
            print("Web site is available")
        else:
            print(f"Web site is not available, count={status_count}")
            status_count = status_count + 1
            if status_count == 5:
                restart_nginx_app(public_ip)
    except Exception as ex:
        print(f"Connection error happened:{ex}")
        # If docker container dies
        restart_nginx_app(public_ip)


def restart_nginx_app(ec2_public_ip_address):
    global status_count
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=ec2_public_ip_address, username='ec2-user',
                       key_filename="/Users/pradeepch/.ssh/docker-server.pem")
    print("Restarting nginx application ...")
    stdin, stdout, stderr = ssh_client.exec_command("docker restart pensive_moore")
    # need to know name of docker container
    print(stdout.readlines())
    ssh_client.close()
    status_count = 0


status_count = 0   # global variable for webapp status check count
lst = create_instance()
add_ingress_rules_to_sg(lst[0])  # pass instance id
install_docker_nginx_on_ec2(lst[1])  # pass ec2 public ip address.
public_dns = get_public_dns(lst[0])  # pass instance id

# install_docker_nginx_on_ec2("52.66.141.18")  # pass ec2 public ip address.
# public_dns = get_public_dns("i-0ec7f83a74c71c224")  # pass instance id

schedule.every(5).seconds.do(nginx_status, public_dns, lst[1])  # pass pubic dns and public ip address.

# schedule.every(5).seconds.do(nginx_status, public_dns, "52.66.141.18")  # pass pubic dns and public ip address.

while True:
    schedule.run_pending()

