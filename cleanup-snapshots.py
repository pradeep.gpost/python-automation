
import boto3
from operator import itemgetter

client = boto3.client('ec2')

response = client.describe_volumes(
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': [
                    'prod',
                ]
            },
        ],
    )

volumes = response['Volumes']

for volume in volumes:

    response = client.describe_snapshots(
        Filters=[
            {
                'Name': 'volume-id',
                'Values': [
                    volume['VolumeId'],
                ]
            },
        ],
        OwnerIds=['self']
    )

    snapshots = response['Snapshots']

    # for snap in snapshots:
    #     snapshotId = snap['SnapshotId']
    #     creationTime = snap['StartTime']
    #     print(f"SnapshotId: {snapshotId}, creationTime : {creationTime}")

    # print("############")

    # Desc order sort
    desc_sorted_snapshots = sorted(snapshots, key=itemgetter('StartTime'), reverse=True)

    for snap in desc_sorted_snapshots:
        print(f"{snap['SnapshotId']}, startTime: {snap['StartTime']}")

    # Ignore deleting first two snapshots
    for snap in desc_sorted_snapshots[2:]:
        response = client.delete_snapshot(
            SnapshotId=snap['SnapshotId'],
        )
        print(response)
