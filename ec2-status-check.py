
import boto3
import schedule

# Takes credentials available for aws (see $aws configure list)
# Default location of credentials if the script is running on laptop is ~/.aws/credentials and ~/.aws/config
client = boto3.client('ec2')

response = client.describe_instance_status(
    IncludeAllInstances=True
)
instances_statuses = response['InstanceStatuses']
# IncludeAllInstances=True makes it check health status of all running or terminated instances.


def check_instance_status():
    for instance_status in instances_statuses:
        instance_id = instance_status['InstanceId']
        instance_state = instance_status['InstanceState']['Name']
        inst_status = instance_status['InstanceStatus']['Status']
        sys_status = instance_status['SystemStatus']['Status']
        print(f"Instance {instance_id} is {instance_state} and instance status is {inst_status} and system status is {sys_status}")

    print("#########################\n")


schedule.every(5).minutes.do(check_instance_status)

while True:
    schedule.run_pending()