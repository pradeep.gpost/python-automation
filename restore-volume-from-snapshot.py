from operator import itemgetter
import boto3

client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')

instance_id = "i-072277a7bb12cbda3"

# Get volumes attached to the mentioned instance
response = client.describe_volumes(
    Filters=[
        {
            'Name': 'attachment.instance-id',
            'Values': [
                instance_id,
            ]
        },
    ]
)

# Assumed to have only one volume for the instance
volume = response['Volumes'][0]

# Get snapshots of the attached volume
response = client.describe_snapshots(
    Filters=[
        {
            'Name': 'volume-id',
            'Values': [
                volume['VolumeId'],
            ]
        },
    ],
    OwnerIds=[
        'self'
    ]
)

snapshots = response['Snapshots']

for snap in snapshots:
    print(f"{snap['SnapshotId']} and {snap['StartTime']}")

latest_snapshot = sorted(snapshots, key=itemgetter('StartTime'), reverse=True)[0]

print(latest_snapshot)

# Create volume from latest snapshot with same tags of attached volume
new_volume = client.create_volume(
    AvailabilityZone="ap-south-1a",  # Same as that of instance az
    SnapshotId=latest_snapshot["SnapshotId"],
    TagSpecifications=[
        {
            'ResourceType': 'volume',
            'Tags': [
                {
                    'Key': 'Name',
                    'Value': 'prod'
                },
            ]
        },
    ],
)

while True:
    volume = ec2_resource.Volume(new_volume['VolumeId'])
    print(volume.state)
    if volume.state == "available":
        # There is a certain delay btw volume creation and attachment till volume becomes available.
        # To overcome this delay, an infinite while loop, with break condition is put together.
        instance = ec2_resource.Instance(instance_id)
        # Device value to be other than the existing volume's value which is /dev/xvda in UI console -
        # - you shall change end letter for different hard disk
        response = instance.attach_volume(
            Device='/dev/xvdb',  # Choose a different device name than was with attached volume
            VolumeId=new_volume['VolumeId']
        )
        break










